大部分代码基于[ColorUI](https://github.com/weilanwl/ColorUI "ColorUI") 修改而来，ColorUI目前只针对微信小程序，本项目ColorUICss时非常纯粹的移动端CSS。 ColorUICss专注于移动端Css框架。初衷主要是要一个纯粹的移动端CSS框架，由于多用于VUE框架中， 基于VUE的框架也有很多优秀的，但往往我只需要其中一个组件，这个时候个项目就会显得很杂乱， 于是就写个纯粹的CSS。

交流QQ群：837450231

感谢[ColorUI](https://github.com/weilanwl/ColorUI "ColorUI") 作者，很喜欢这样的色彩搭配。

[手机演示](http://anyhome.gitee.io/colorui/ "http://anyhome.gitee.io/colorui/")

[![](https://oscimg.oschina.net/oscnet/2d28d0b43f531ccb2edad7e31367676a61d.jpg)](https://oscimg.oschina.net/oscnet/2d28d0b43f531ccb2edad7e31367676a61d.jpg)

- 演示安装

```bash
git clone https://gitee.com/anyhome/ColorUI.git
cd ColorUI 
npm install
npm run serve
```
**浏览地址 http://localhost:9003/colorui/**




## 基础

1.  布局
2.  背景
3.  文本
4.  图标
5.  按钮
6.  标签
7.  头像
8.  进度条
9.  边框阴影
10.  加载

## 组件

1.  操作条
2.  导航栏
3.  列表
4.  卡片
5.  输入框 6.复选框 7.单选框 8.开关
6.  时间轴
7.  聊天
8.  轮播
9.  模态框
10.  步骤条

# 概述

ColorUICss专注于移动端Css框架，主要用于在VUE框架中弥补某些小功能、小组件的不足之处，ColorUICss只是一个纯粹的移动端CSS。
多处采用 **tien-** 作为命名空间而不是所有地方，是为了再不影响其他css命名冲突的前提下，一些常用的组件又不至于书写麻烦。组件的命名空间后期可能还会继续更新。

# 布局

## 固定尺寸

采用flex弹性布局，安装宽带百分比显示，例如显示20%宽度。
```html
<div class="flex flex-wrap">
	<div class='flex-20 bg-grey margin-xs padding-sm radius'>xs(20%)</div>
	<div class='flex-40'></div>
</div>
```
```css
.flex {
  display: flex;
}
.flex-wrap {
  flex-wrap: wrap;
}
.flex-20 {
  flex-basis: 20%;
}


.flex-40 {
  flex-basis: 40%;
}
.flex-50 {
  flex-basis: 50%;
}
.flex-60 {
  flex-basis: 60%;
}
.flex-80 {
  flex-basis: 80%;
}

```
![](https://oscimg.oschina.net/oscnet/36b619d156302b5b8895991055b34338e40.jpg)

## 比例布局
```html
      <div class="flex">
        <div class='flex-sub bg-grey padding-sm margin-xs radius'>1</div>
        <div class='flex-sub bg-grey padding-sm margin-xs radius'>1</div>
      </div>
      <div class="flex margin-bottom-sm mb-sm">
        <div class='flex-sub bg-grey padding-sm margin-xs radius'>1</div>
        <div class='flex-twice bg-grey padding-sm margin-xs radius'>2</div>
      </div>
      <div class="flex  margin-bottom-sm mb-sm">
        <div class='flex-1 bg-grey padding-sm margin-xs radius'>1</div>
        <div class='flex-2 bg-grey padding-sm margin-xs radius'>2</div>
        <div class='flex-3 bg-grey padding-sm margin-xs radius'>3</div>
      </div>
```
```css
.flex {
  display: flex;
}
.flex-1 {
  flex: 1;
}
.flex-2 {
  flex: 2;
}
```
![](https://oscimg.oschina.net/oscnet/354bd4ea8989cc003535143bc51e04177b0.jpg)

- 水平对齐 justify
```html
<div class="flex justify-start">
  <div >start</div>
  <div >start</div>
</div>
<div class="flex justify-end">
  <div >end</div>
  <div >end</div>
</div>
<div class="flex justify-center">
  <div >center</div>
  <div >center</div>
</div>
<div class="flex justify-between">
  <div >between</div>
  <div >between</div>
</div>
<div class="flex justify-around">
  <div >around</div>
  <div >around</div>
</div>
```

```css
.justify-start {
  justify-content: flex-start;
}
.justify-end {
  justify-content: flex-end;
}
.justify-center {
  justify-content: center;
}
.justify-between {
  justify-content: space-between;
}
.justify-around {
  justify-content: space-around;
}

```
![](https://oscimg.oschina.net/oscnet/def55468a1757a93425f2b528fd1e792db1.jpg)

- 垂直对齐 align

```html
<div class="flex  align-start">
  <div >ColorUi</div>
  <div >start</div>
</div>
<div class="flex  align-end">
  <div >ColorUi</div>
  <div >end</div>
</div>
<div class="flex  align-center">
  <div >ColorUi</div>
  <div >center</div>
</div>
```
```css
.align-start {
align-items: flex-start;
}
.align-end {
align-items: flex-end;
}
.align-center {
align-items: center;
}
.align-stretch {
align-items: stretch;
}
.align-stretch {
align-items: stretch;
}
```
![](https://oscimg.oschina.net/oscnet/1f1ba282634ab4c240a7ab4271cb58dcab0.jpg)

## Grid布局
整个分10列，和我们通常使用的btcss还不太一样，比如
```html
<div class="margin-bottom text-center grid col-4">
  <div class=" padding tien-col ">1</div>
  <div class=" padding tien-col ">2</div>
  <div class=" padding tien-col ">3</div>
  <div class=" padding tien-col ">4</div>
</div>

<div class="margin-bottom text-center grid col-5">
  <div class=" padding tien-col ">1</div>
  <div class=" padding tien-col ">2</div>
  <div class=" padding tien-col ">3</div>
  <div class=" padding tien-col ">4</div>
  <div class=" padding tien-col ">5</div>
  <div class=" padding tien-col ">6</div>
  <div class=" padding tien-col ">7</div>
  <div class=" padding tien-col ">8</div>
  <div class=" padding tien-col ">9</div>
  <div class=" padding tien-col ">10</div>
</div>
```
当我们要实现4列布局时，需要加外层 grid col-4,自动将内层tien-col等分。


![](https://oscimg.oschina.net/oscnet/2032d49864ee9dbbaf53f35148e46b399e1.jpg)


- 等高
```html
<div class="grid col-4 grid-square">
</div>
```
```css
.grid.grid-square {
  overflow: hidden;
}
```


## 辅助布局
- 边距

margin-no  所有外边距为 0px

margin-5 所有外边距为5px，其他还有 margin-10、margin-15、margin-20、margin-25

margin-top-no 外上边距为 0 px，其他还有 margin-top-5、margin-top-10、margin-top-15、margin-top-20、margin-top-25

类似的还有左边距 margin-left、右边距margin-right、下边距margin-bottom。
内边距以此类推。如 padding-no、padding-5、padding-10

默认margin、padding 为 15px;

```html
<div class="bg-white">
      <div class='padding bg-gray'>{size}的尺寸有xs/sm/df/lg/xl</div>
      <div class='flex flex-wrap padding border-top'>
        <div class="basis-df padding-bottom-xs">外边距</div>
        <div class="basis-df padding-bottom-xs">内边距</div>
        <div class="basis-df">.margin-{size}</div>
        <div class="basis-df">.padding-{size}</div>
      </div>
      <div class='flex flex-wrap padding border-top'>
        <div class="basis-df padding-bottom-xs">水平方向外边距</div>
        <div class="basis-df padding-bottom-xs">水平方向内边距</div>
        <div class="basis-df">.margin-lr-{size}</div>
        <div class="basis-df">.padding-lr-{size}</div>
      </div>
      <div class='flex flex-wrap padding border-top'>
        <div class="basis-df padding-bottom-xs">垂直方向外边距</div>
        <div class="basis-df padding-bottom-xs">垂直方向内边距</div>
        <div class="basis-df">.margin-tb-{size}</div>
        <div class="basis-df">.padding-tb-{size}</div>
      </div>
      <div class='flex flex-wrap padding border-top'>
        <div class="basis-df padding-bottom-xs">上外边距</div>
        <div class="basis-df padding-bottom-xs">上内边距</div>
        <div class="basis-df">.margin-top-{size}</div>
        <div class="basis-df">.padding-top-{size}</div>
      </div>
      <div class='flex flex-wrap padding border-top'>
        <div class="basis-df padding-bottom-xs">右外边距</div>
        <div class="basis-df padding-bottom-xs">右内边距</div>
        <div class="basis-df">.margin-right-{size}</div>
        <div class="basis-df">.padding-right-{size}</div>
      </div>
      <div class='flex flex-wrap padding border-top'>
        <div class="basis-df padding-bottom-xs">下外边距</div>
        <div class="basis-df padding-bottom-xs">下内边距</div>
        <div class="basis-df">margin-bottom-{size}</div>
        <div class="basis-df">.padding-bottom-{size}</div>
      </div>
      <div class='flex flex-wrap padding border-top'>
        <div class="basis-df padding-bottom-xs">左外边距</div>
        <div class="basis-df padding-bottom-xs">左内边距</div>
        <div class="basis-df">.margin-left-{size}</div>
        <div class="basis-df">.padding-left-{size}</div>
      </div>
    </div>
```


- 圆角和浮动

```css
//默认圆角
.radius {
  border-radius: 3px;
}
//宽度100%
.response {
  width: 100%;
}

/* -- 浮动 --  */
.cf::after, .cf::before,.clear::after, .clear::before {
  content: " ";
  display: table;
}
.cf::after,.clear::after {
  clear: both;
}
.fl {
  float: left;
}
.fr {
  float: right;
}
```

# 背景
背景以bg-前缀，例如红色背景bg-red，其他还有.bg-red、.bg-orange、.bg-yellow、.bg-olive、.bg-green、.bg-cyan、.bg-blue、.bg-purple、.bg-mauve、.bg-pink、.bg-brown、.bg-grey、.bg-gray、.bg-black、.bg-white。加上 .light  则是相对应的浅色背景

![](https://oscimg.oschina.net/oscnet/4c582bf0db212ebde52e0e6d3233da23578.jpg)![](https://oscimg.oschina.net/oscnet/5f2e2e97ce23cbb3f7c9850d978edc71ae7.jpg)

- 渐变背景
```html
<div>
  <div class='bg-gradual-red  shadow-blur'>
    <div class="text-lg">魅红</div>
    <div class='margin-top-sm text-Abc'>#f43f3b - #ec008c</div>
  </div>
</div>
<div>
  <div class='bg-gradual-orange  shadow-blur'>
    <div class="text-lg">鎏金</div>
    <div class='margin-top-sm text-Abc'>#ff9700 - #ed1c24</div>
  </div>
</div>
<div>
  <div class='bg-gradual-green  shadow-blur'>
    <div class="text-lg">翠柳</div>
    <div class='margin-top-sm text-Abc'>#39b54a - #8dc63f</div>
  </div>
</div>
<div>
  <div class='bg-gradual-blue  shadow-blur'>
    <div class="text-lg">靛青</div>
    <div class='margin-top-sm text-Abc'>#0081ff - #1cbbb4</div>
  </div>
</div>
<div>
  <div class='bg-gradual-purple  shadow-blur'>
    <div class="text-lg">惑紫</div>
    <div class='margin-top-sm text-Abc'>#9000ff - #5e00ff</div>
  </div>
</div>
<div class=-sm'>
  <div class='bg-gradual-pink  shadow-blur'>
    <div class="text-lg">霞彩</div>
    <div class='margin-top-sm text-Abc'>#ec008c - #6739b6</div>
  </div>
</div>
```

![](https://oscimg.oschina.net/oscnet/c08a66b7ee5eecbc0e43581310672a450f2.jpg)


# 文本
- 文字大小

```css
.text-10 {
  font-size: 10px;
}
.text-12 {
  font-size: 12px;
}
.text-14 {
  font-size: 14px;
}
.text-16 {
  font-size: 16px;
}
.text-18 {
  font-size: 18px;
}
.text-22 {
  font-size: 22px;
}
.text-40 {
  font-size: 40px;
}
.text-60 {
  font-size: 60px;
}
//首字母大写
.text-Abc {
  text-transform: Capitalize;
}
//字母全部大写
.text-ABC {
  text-transform: Uppercase;
}
//字母全部小写
.text-abc {
  text-transform: Lowercase;
}
//字符截断
.text-cut {
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
}
//粗体
.text-bold {
  font-weight: bold;
}
```
![](https://oscimg.oschina.net/oscnet/71f67a73f702ad1bcded63d0239962528f7.jpg)

- 文字对齐
```css
//居中
.text-center {
  text-align: center;
}
//行高
.text-content {
  line-height: 1.6;
}
//左对齐
.text-left {
  text-align: left;
}
//右对齐
.text-right {
  text-align: right;
}
```
![](https://oscimg.oschina.net/oscnet/bbd914c3c2569d4f46e7ea156a58f79a65e.jpg)

文字颜色
```css
.text-red, .line-red, .lines-red {
  color: #e54d42;
}
.text-orange, .line-orange, .lines-orange {
  color: #f37b1d;
}
.text-yellow, .line-yellow, .lines-yellow {
  color: #fbbd08;
}
.text-olive, .line-olive, .lines-olive {
  color: #8dc63f;
}
.text-green, .line-green, .lines-green {
  color: #39b54a;
}
.text-cyan, .line-cyan, .lines-cyan {
  color: #1cbbb4;
}
.text-blue, .line-blue, .lines-blue {
  color: #0081ff;
}
.text-purple, .line-purple, .lines-purple {
  color: #6739b6;
}
.text-mauve, .line-mauve, .lines-mauve {
  color: #9c26b0;
}
.text-pink, .line-pink, .lines-pink {
  color: #e03997;
}
.text-brown, .line-brown, .lines-brown {
  color: #a5673f;
}
.text-grey, .line-grey, .lines-grey {
  color: #8799a3;
}
.text-gray, .line-gray, .lines-gray {
  color: #aaa;
}
.text-black, .line-black, .lines-black {
  color: #333;
}
.text-white, .line-white, .lines-white {
  color: #fff;
}
```
![](https://oscimg.oschina.net/oscnet/9765069d65738d4431650d742e71960d3d2.jpg)

# 图标
图标使用阿里云 iconfont图标，为不和自己的图标冲突使用tien-前缀，例如 .tien-icon-fork
```css
[class*="tien-icon-"] {
  font-family: "iconfont" !important;
  font-size: inherit;
  font-style: normal;
}
```

![](https://oscimg.oschina.net/oscnet/792edbb122870b9adf29db62e5922b7879b.jpg)


# 按钮
- 形状、尺寸、颜色

颜色请参考背景颜色
```html
<button class="tien-btn">默认</button>
<button class="tien-btn round">圆角</button>
<button class="tien-btn icon">
	<span class='tien-icon-emojifill'>图标</span>
</button>

<button class='tien-btn round sm'>小尺寸</button>
<button class='tien-btn round'>默认</button>
<button class='tien-btn round lg'>大尺寸</button>
```
![](https://oscimg.oschina.net/oscnet/0f7f2640286af55f875b74d9544539d8a0a.jpg)

镂空按钮只需要加 .line-[背景色],块状按钮加  .lg

![](https://oscimg.oschina.net/oscnet/bbc9d758c955aa892c3953a004b306a6487.jpg)


# 标签

- 形状、尺寸、颜色
```html
<div class="tien-tag ">默认</div>
<div class="tien-tag round ">椭圆</div>
<div class="tien-tag radius">圆角</div>

<div class='tien-tag radius sm '>小尺寸</div>
<div class='tien-tag radius '>普通尺寸</div>

颜色则加 bg-[背景色]  例如 bg-red 参考背景色

```
![](https://oscimg.oschina.net/oscnet/801e20c234ec2754f9b7a7a9db24ff9f3bf.jpg)

- 样式
镂空加 line-[背景色]  例如 bg-red 参考背景色。胶囊样式
```html
<div class="tien-capsule">
  <div class='tien-tag bg-red'>
    <span class='tien-icon-likefill'></span>
  </div>
  <div class="tien-tag line-red">
    12
  </div>
</div>
<div class="tien-capsule round">
  <div class='tien-tag bg-blue '>
    <span class='tien-icon-likefill'></span>
  </div>
  <div class="tien-tag line-blue">
    23
  </div>
</div>
<div class="tien-capsule round">
  <div class='tien-tag bg-blue '>
    说明
  </div>
  <div class="tien-tag line-blue">
    123
  </div>
</div>
<div class="tien-capsule radius">
  <div class='tien-tag bg-grey '>
    <span class='tien-icon-likefill'></span>
  </div>
  <div class="tien-tag line-grey">
    23
  </div>
</div>
<div class="tien-capsule radius">
  <div class='tien-tag bg-brown sm'>
    <span class='tien-icon-likefill'></span>
  </div>
  <div class="tien-tag line-brown sm">
    23
  </div>
</div>
      
```
![](https://oscimg.oschina.net/oscnet/1da66f4cc546f8af8f2753b2bf68bcaf58c.jpg)





如果本项目对您有帮助请给我点动力^_^ 

![支付宝](https://oscimg.oschina.net/oscnet/65ceb79f30bc037bfe8076923f506cb95f7.jpg)![微信](https://oscimg.oschina.net/oscnet/0c5be86b05fc90bbf8499a5badda8ba9c72.jpg)